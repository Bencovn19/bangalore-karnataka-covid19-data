
# Bangalore Data

### * Bangalore Daily Cases - Deaths - Testing Nos - Doubling times - Death % - Test Positivity Rates.csv

* Daily Total Case Nos.
* Daily New Case Nos.
* Daily Total Death Nos.
* Daily New Death Nos.
* Death %: (= Total Death No. / Total Case No. * 100)
* Doubling Time (Days) (5 day Avg.)[1]
* Reproduction Number (R, Ro)[2] (Probably extremely inaccurate)
* Daily Cumulative Testing Nos. (Testing Numbers for a day are released the next day)
* Test Positivity Rate (+ve per Tested [Total Cases / Total Tests] (%))

### * Bangalore - Zone-wise - Daily Cases.csv

   **Daily Cases for the following Bangalore Zones:**[4]
* East
* West
* South
* Bommanahalli
* Yelahanka
* RR Nagar
* Dasarahalli
* Mahadevapura
* Rest of Bangalore

### * Bangalore - Zone-wise - Daily Deaths.csv

   **Daily Deaths for the following Bangalore Zones:**[4]
* East
* West
* South
* Bommanahalli
* Yelahanka
* RR Nagar
* Dasarahalli
* Mahadevapura
* Rest of Bangalore

# Karnataka Data

### * Karnataka Daily Cases - Deaths - Testing Nos - Doubling time - Test Positivity Rates.csv

* Daily Total Case Nos.
* Daily New Case Nos.
* Daily Total Death Nos.
* Daily New Death Nos.
* Death %: (= Total Death No. / Total Case No. * 100)
* Doubling Time (Days) (5 day Avg.)[1]
* Reproduction Number (R, Ro)[2] (Probably extremely inaccurate)
* Daily Cumulative Testing Nos.
* Test Positivity Rate (+ve per Tested [Total Cases / Total Tests] (%))

# India State-wise Daily Data

### * State-wise Daily Cases.csv

* Daily Total Case Nos. for each State

### * State-wise Daily Deaths.csv

* Daily Total Death Nos. for each State

### * State-wise Daily Doubling Times (5 Day Avg.).csv

* Daily Doubling Times (in days) for each State (5 Day Average)[1]

### * State-wise Daily Death %.csv

* Daily State-wise Death(%) (=Total Death No. / Total Case No.*100)

# India Total Daily Data

### * India Daily Cases - Deaths - Testing Nos - Doubling time - Test Positivity Rates.csv

* Case and Death numbers From WHO Situation Reports[5]
* Daily Total Case Nos.
* Daily New Case Nos.
* Daily Total Death Nos.
* Daily New Death Nos.
* Death %: (= Total Death No. / Total Case No. * 100)
* Doubling Time (Days) (5 day Avg.)[1]
* Reproduction Number (R, Ro)[2] (Probably extremely inaccurate)
* Daily Cumulative Testing Nos. (Testing Numbers for a day are released the next day by at 9AM)
* Test Positivity Rate (+ve per Tested [Total Cases / Total Tests] (%))

# Sources:

**WHO Situation Reports:**

https://www.who.int/emergencies/diseases/novel-coronavirus-2019/situation-reports/

**India - Ministry of Health and Family Welfare - Daily State-wise updates:**

https://www.mohfw.gov.in/

**ICMR Daily Testing Nos.:**

https://www.icmr.gov.in

**Karnataka Daily Health Bulletin:**

https://covid19.karnataka.gov.in/new-page/Health%20Department%20Bulletin/en

**Karnataka Daily War Room Bulletin:**

https://covid19.karnataka.gov.in/new-page/Daily%20Bulletin/en

**Karnataka Daily War Room Score Card:**

https://covid19.karnataka.gov.in/new-page/Score%20Card/en

**BBMP Daily War Room Bulletin:**

https://covid19.karnataka.gov.in/new-page/BBMP%20War%20Room%20Bulletin/en

**Daily Bangalore Ward-wise Infection Maps:**

http://dev.ksrsac.in/coviddocs/

**Karnataka State War Room Bulletins:**

https://covid19.karnataka.gov.in/sate_war_room_bulletin/en

**Karnataka State Bulletins (Health Dept Bulletins, BBMP War Room Bulletins, State War Room Bulletins)**

https://covid19.karnataka.gov.in/govt_bulletin/en

**COVID Maps (Frequently Outdated):**

https://covid19.karnataka.gov.in/imp_maps/en

# References

[1] [Doubling time-Wikipedia](https://en.wikipedia.org/wiki/Doubling_time)

[2] https://gitlab.com/Bencovn19/bangalore-karnataka-covid19-data/-/blob/master/Source%20Documents%20Archive/Other%20Files/Ro%20Calculation.txt

[3] [How To Tell If We're Beating COVID-19](https://www.youtube.com/watch?v=54XLXg4fYsc)

[4] https://gitlab.com/Bencovn19/bangalore-karnataka-covid19-data/-/blob/master/Source%20Documents%20Archive/Bangalore/Other%20files/Ward%20No.%20-%20Ward%20Name%20-%20Zone%20-%20Constituency.xls

[5] https://www.who.int/emergencies/diseases/novel-coronavirus-2019/situation-reports/


# Karnataka Government Orders and Circulars:

**General Information:**

https://covid19.karnataka.gov.in/new-page/GENERAL%20INFORMATION/en

**Government Orders:**

https://covid19.karnataka.gov.in/new-page/Government%20Orders/en

**Government Circulars:**

https://covid19.karnataka.gov.in/new-page/Government%20Circulars/en
